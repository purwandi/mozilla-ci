#!/bin/bash

set -e
cd src/

echo "Checking Env"
node --version
npm --version

## Linting
npm run lint

